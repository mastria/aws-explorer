module gitlab.com/ongresinc/labs/aws-explorer

go 1.13

require (
	github.com/alexeyco/simpletable v0.0.0-20200203113705-55bd62a5b8df
	github.com/aws/aws-sdk-go v1.30.20
	github.com/aws/aws-sdk-go-v2 v0.22.0 // indirect
	github.com/dustin/go-humanize v1.0.0
	github.com/gosuri/uilive v0.0.4
	github.com/gosuri/uiprogress v0.0.1
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.6.3
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	golang.org/x/sys v0.0.0-20200501145240-bc7a7d42d5c3 // indirect
)
