package explorer

import "github.com/aws/aws-sdk-go/aws/session"

type collectFunc func(string, *session.Session) ([]interface{}, error)

var actions = []collectFunc{
	getInstances,
	getVolumes,
	getSnapshots,
	getIPs,
	getNatGateways,
	getEKSClusters,
	getLoadbalancers,
	getRDSInstances,
	getRDSSnapshots,
}

func populateData(r *Report, data []interface{}) {

	if len(data) == 0 {
		return
	}

	switch data[0].(type) {
	case *Instance:
		for _, i := range data {
			r.Instances = append(r.Instances, *i.(*Instance))
		}
	case *DiskVolume:
		for _, i := range data {
			r.Volumes = append(r.Volumes, *i.(*DiskVolume))
		}
	case *DiskSnapshot:
		for _, i := range data {
			r.Snapshots = append(r.Snapshots, *i.(*DiskSnapshot))
		}
	case *ElasticIP:
		for _, i := range data {
			r.ElaticIPs = append(r.ElaticIPs, *i.(*ElasticIP))
		}
	case *NATGateway:
		for _, i := range data {
			r.NATGateways = append(r.NATGateways, *i.(*NATGateway))
		}
	case *EKSCluster:
		for _, i := range data {
			r.EksClusters = append(r.EksClusters, *i.(*EKSCluster))
		}
	case *Loadbalancer:
		for _, i := range data {
			r.Loadbalancers = append(r.Loadbalancers, *i.(*Loadbalancer))
		}
	case *RDSInstance:
		for _, i := range data {
			r.RDSInstances = append(r.RDSInstances, *i.(*RDSInstance))
		}
	case *RDSSnapshot:
		for _, i := range data {
			r.RDSSnapshots = append(r.RDSSnapshots, *i.(*RDSSnapshot))
		}
	}
}
