package explorer

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/rds"
)

// RDSInstance is a RDS instance on AWS
type RDSInstance struct {
	Region   string
	ARN      string
	Name     string
	Class    string
	Status   string
	Engine   string
	Version  string
	DiskSize int64
}

func getRDSInstances(region string, s *session.Session) ([]interface{}, error) {
	var out []interface{}

	svc := rds.New(s)
	result, err := svc.DescribeDBInstances(nil)

	if err != nil {
		return nil, fmt.Errorf("could not get instances: %v", err)
	}

	for _, r := range result.DBInstances {
		out = append(out, &RDSInstance{
			Region:   region,
			ARN:      aws.StringValue(r.DBInstanceArn),
			Name:     aws.StringValue(r.DBClusterIdentifier),
			Class:    aws.StringValue(r.DBInstanceClass),
			Status:   aws.StringValue(r.DBInstanceStatus),
			Engine:   aws.StringValue(r.Engine),
			Version:  aws.StringValue(r.EngineVersion),
			DiskSize: aws.Int64Value(r.AllocatedStorage) * GiB,
		})
	}

	return out, nil
}
