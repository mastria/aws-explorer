package explorer

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

// ElasticIP is a EIP in AWS
type ElasticIP struct {
	Region     string
	PublicIP   string
	Tags       map[string]string
	InstanceID string
}

func getIPs(region string, s *session.Session) ([]interface{}, error) {
	var out []interface{}

	svc := ec2.New(s)

	result, err := svc.DescribeAddresses(nil)

	if err != nil {
		return nil, fmt.Errorf("could not get the EIPs: %v", err)
	}

	for _, ip := range result.Addresses {

		out = append(out, &ElasticIP{
			Region:     region,
			PublicIP:   *ip.PublicIp,
			Tags:       parseTags(ip.Tags),
			InstanceID: aws.StringValue(ip.InstanceId),
		})
	}

	return out, nil
}
