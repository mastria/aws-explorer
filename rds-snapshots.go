package explorer

import (
	"fmt"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/rds"
)

// RDSSnapshot is a RDS snapshot on AWS
type RDSSnapshot struct {
	Region   string
	Size     int64
	Instance string
	ID       string
	Class    string
	Status   string
	Engine   string
	Version  string
	Creation time.Time
}

func getRDSSnapshots(region string, s *session.Session) ([]interface{}, error) {
	var out []interface{}

	svc := rds.New(s)
	result, err := svc.DescribeDBSnapshots(nil)

	if err != nil {
		return nil, fmt.Errorf("could not get instances: %v", err)
	}

	for _, r := range result.DBSnapshots {
		out = append(out, &RDSSnapshot{
			Region:   region,
			Instance: aws.StringValue(r.DBInstanceIdentifier),
			ID:       aws.StringValue(r.DBSnapshotIdentifier),
			Size:     aws.Int64Value(r.AllocatedStorage) * GiB,
			Engine:   aws.StringValue(r.Engine),
			Version:  aws.StringValue(r.EngineVersion),
			Status:   aws.StringValue(r.Status),
			Creation: aws.TimeValue(r.SnapshotCreateTime),
		})
	}

	return out, nil
}
