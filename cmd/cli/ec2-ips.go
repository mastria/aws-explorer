package main

import (
	"github.com/alexeyco/simpletable"
	explorer "gitlab.com/ongresinc/labs/aws-explorer"
)

func eipToCell(i explorer.ElasticIP) []*simpletable.Cell {
	return []*simpletable.Cell{
		{Align: simpletable.AlignCenter, Text: i.Region},
		{Align: simpletable.AlignCenter, Text: i.PublicIP},
		{Align: simpletable.AlignCenter, Text: i.InstanceID},
		{Align: simpletable.AlignLeft, Text: processTags(i.Tags)},
	}
}

func printEIPs(ips []explorer.ElasticIP) {

	table := simpletable.New()

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "Region"},
			{Align: simpletable.AlignCenter, Text: "IP"},
			{Align: simpletable.AlignCenter, Text: "InstanceID"},
			{Align: simpletable.AlignCenter, Text: "Tags"},
		},
	}

	for _, v := range ips {
		table.Body.Cells = append(table.Body.Cells, eipToCell(v))
	}

	printTable("All Elastic IPs", table)
}
