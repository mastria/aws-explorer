package main

import (
	"github.com/alexeyco/simpletable"
	explorer "gitlab.com/ongresinc/labs/aws-explorer"
)

func eksToCell(e explorer.EKSCluster) []*simpletable.Cell {
	return []*simpletable.Cell{
		{Align: simpletable.AlignCenter, Text: e.Region},
		{Align: simpletable.AlignCenter, Text: e.Name},
		{Align: simpletable.AlignCenter, Text: e.Arn},
		{Align: simpletable.AlignCenter, Text: e.Status},
		{Align: simpletable.AlignCenter, Text: e.Version},
		{Align: simpletable.AlignLeft, Text: processTags(e.Tags)},
	}
}

func printEKSClusters(ips []explorer.EKSCluster) {

	table := simpletable.New()

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "Region"},
			{Align: simpletable.AlignCenter, Text: "Name"},
			{Align: simpletable.AlignCenter, Text: "ARN"},
			{Align: simpletable.AlignCenter, Text: "Status"},
			{Align: simpletable.AlignCenter, Text: "Version"},
			{Align: simpletable.AlignCenter, Text: "Tags"},
		},
	}

	for _, v := range ips {
		table.Body.Cells = append(table.Body.Cells, eksToCell(v))
	}

	printTable("All EKS Clusters", table)
}
