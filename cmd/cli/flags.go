package main

import (
	"fmt"
	"os"

	flag "github.com/spf13/pflag"
)

const appName = "aws-explorer"

var (
	accessKeyCMD    string
	accessSecretCMD string
	regionCMD       string
)

func init() {

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "%s lists all objects created in AWS on all available regions.\n\n", appName)
		fmt.Fprintf(os.Stderr, "Usage:\n")
		fmt.Fprintf(os.Stderr, "  %s [OPTIONS]...\n\n", appName)
		fmt.Fprintf(os.Stderr, "General Options:\n")

		flag.PrintDefaults()
		fmt.Fprintf(os.Stderr, "\nReport BUGs on https://gitlab.com/ongresinc/labs/aws-explorer.\n\n")
	}

	flag.StringVar(&accessKeyCMD, "access-key", "", "AWS access key")
	flag.StringVar(&accessSecretCMD, "access-secret", "", "AWS access secret")
	flag.StringVar(&regionCMD, "region", "us-east-1", "AWS region")

	flag.Parse()

	// if accessKeyCMD == "" || accessSecretCMD == "" {
	// 	fmt.Println("Missing AWS Credentials")
	// 	os.Exit(2)
	// }

}
