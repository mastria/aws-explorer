package main

import (
	"fmt"

	"github.com/alexeyco/simpletable"
)

func printTable(header string, table *simpletable.Table) {
	fmt.Println()
	fmt.Println(header)

	var msg = " >> No data found."

	if totalRows := len(table.Body.Cells); totalRows > 0 {

		spanSize := len(table.Header.Cells) - 1

		if spanSize < 0 {
			spanSize = 1
		}

		table.Footer = &simpletable.Footer{
			Cells: []*simpletable.Cell{
				{Align: simpletable.AlignCenter, Text: "total count"},
				{Span: spanSize, Align: simpletable.AlignLeft, Text: fmt.Sprintf("%d", totalRows)},
			},
		}

		msg = table.String()
	}

	fmt.Println(msg)
}
