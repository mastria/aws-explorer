package main

import (
	"fmt"

	"github.com/alexeyco/simpletable"
	"github.com/dustin/go-humanize"
	explorer "gitlab.com/ongresinc/labs/aws-explorer"
)

func rdsSnapToCell(r explorer.RDSSnapshot) []*simpletable.Cell {
	return []*simpletable.Cell{
		{Align: simpletable.AlignCenter, Text: r.Region},
		{Align: simpletable.AlignCenter, Text: r.ID},
		{Align: simpletable.AlignCenter, Text: r.Status},
		{Align: simpletable.AlignCenter, Text: r.Engine},
		{Align: simpletable.AlignLeft, Text: r.Version},
		{Align: simpletable.AlignLeft, Text: humanize.IBytes(uint64(r.Size))},
		{Align: simpletable.AlignLeft, Text: fmt.Sprintf("%v", r.Creation)},
	}
}

func printRDSSnapshot(s []explorer.RDSSnapshot) {

	table := simpletable.New()

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "Region"},
			{Align: simpletable.AlignCenter, Text: "ID"},
			{Align: simpletable.AlignCenter, Text: "State"},
			{Align: simpletable.AlignCenter, Text: "Engine"},
			{Align: simpletable.AlignCenter, Text: "Version"},
			{Align: simpletable.AlignCenter, Text: "Size"},
			{Align: simpletable.AlignCenter, Text: "CreatedAt"},
		},
	}

	var allocSize int64
	for _, i := range s {
		table.Body.Cells = append(table.Body.Cells, rdsSnapToCell(i))

		allocSize += i.Size
	}

	printTable("All RDS Snapshots", table)

	if allocSize > 0 {
		fmt.Printf("Total size: %s\n", humanize.IBytes(uint64(allocSize)))
	}
}
