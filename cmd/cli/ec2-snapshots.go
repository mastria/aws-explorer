package main

import (
	"fmt"

	"github.com/alexeyco/simpletable"
	"github.com/dustin/go-humanize"
	explorer "gitlab.com/ongresinc/labs/aws-explorer"
)

func snapToCell(s explorer.DiskSnapshot) []*simpletable.Cell {
	return []*simpletable.Cell{
		{Align: simpletable.AlignCenter, Text: s.Region},
		{Align: simpletable.AlignCenter, Text: s.ID},
		{Align: simpletable.AlignCenter, Text: s.State},
		{Align: simpletable.AlignCenter, Text: s.VolumeID},
		{Align: simpletable.AlignLeft, Text: humanize.IBytes(uint64(s.VolumeSize))},
		{Align: simpletable.AlignCenter, Text: s.OwnerID},
		{Align: simpletable.AlignCenter, Text: s.OwnerAlias},
	}
}

func printSnapshots(snapshots []explorer.DiskSnapshot) {

	table := simpletable.New()

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "Region"},
			{Align: simpletable.AlignCenter, Text: "SnapshotID"},
			{Align: simpletable.AlignCenter, Text: "State"},
			{Align: simpletable.AlignCenter, Text: "VolumeID"},
			{Align: simpletable.AlignCenter, Text: "VolumeSize"},
			{Align: simpletable.AlignCenter, Text: "OwnerID"},
			{Align: simpletable.AlignCenter, Text: "OwnerAlias"},
		},
	}

	var sizeOverall uint64

	for _, snap := range snapshots {
		table.Body.Cells = append(table.Body.Cells, snapToCell(snap))
		sizeOverall += uint64(snap.VolumeSize)
	}

	printTable("All EC2 Snapshots", table)
	if len(table.Body.Cells) > 0 {
		fmt.Printf("Total size: %s\n", humanize.IBytes(sizeOverall))
	}
}
