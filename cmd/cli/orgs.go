package main

import (
	"github.com/alexeyco/simpletable"
	explorer "gitlab.com/ongresinc/labs/aws-explorer"
)

func accToCell(a explorer.Account) []*simpletable.Cell {
	return []*simpletable.Cell{
		{Align: simpletable.AlignLeft, Text: a.ID},
		{Align: simpletable.AlignLeft, Text: a.Name},
		{Align: simpletable.AlignCenter, Text: a.Status},
		{Align: simpletable.AlignLeft, Text: a.ARN},
	}
}

func printAccounts(list explorer.Accounts) {

	table := simpletable.New()

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "ID"},
			{Align: simpletable.AlignCenter, Text: "Name"},
			{Align: simpletable.AlignCenter, Text: "Status"},
			{Align: simpletable.AlignCenter, Text: "ARN"},
		},
	}

	for _, a := range list {
		table.Body.Cells = append(table.Body.Cells, accToCell(a))
	}

	printTable("All Organization accounts", table)
}
