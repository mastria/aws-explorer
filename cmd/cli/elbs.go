package main

import (
	"strings"

	"github.com/alexeyco/simpletable"
	explorer "gitlab.com/ongresinc/labs/aws-explorer"
)

func lbToCell(elb explorer.Loadbalancer) []*simpletable.Cell {
	return []*simpletable.Cell{
		{Align: simpletable.AlignCenter, Text: elb.Region},
		{Align: simpletable.AlignCenter, Text: elb.Name},
		{Align: simpletable.AlignCenter, Text: elb.VpcID},
		{Align: simpletable.AlignCenter, Text: elb.Type},
		{Align: simpletable.AlignLeft, Text: strings.Join(elb.SecurityGroups, "\n")},
		{Align: simpletable.AlignLeft, Text: strings.Join(elb.AvailabilityZones, "\n")},
	}
}

func printLoadbalancers(ips []explorer.Loadbalancer) {

	table := simpletable.New()

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "Region"},
			{Align: simpletable.AlignCenter, Text: "Name"},
			{Align: simpletable.AlignCenter, Text: "VPC"},
			{Align: simpletable.AlignCenter, Text: "Type"},
			{Align: simpletable.AlignCenter, Text: "Security Groups"},
			{Align: simpletable.AlignCenter, Text: "AZs"},
		},
	}

	for _, v := range ips {
		table.Body.Cells = append(table.Body.Cells, lbToCell(v))
	}

	printTable("All Elastic Load Balancers", table)
}
