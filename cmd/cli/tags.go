package main

import (
	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go/service/ec2"
)

func getKey(m map[string]string, k string) string {
	if val, ok := m[k]; ok {
		return val
	}

	return "~"
}

func parseTags(tags []*ec2.Tag) map[string]string {

	var out = make(map[string]string)

	for _, tag := range tags {
		out[*tag.Key] = *tag.Value
	}

	return out
}

func processTags(tags map[string]string) string {

	var tagList []string

	for k, v := range tags {
		tagList = append(tagList, fmt.Sprintf("%s = %s", k, v))
	}

	return strings.Join(tagList, "\n")
}
