package main

import (
	"fmt"
	"strings"

	"github.com/alexeyco/simpletable"
	"github.com/dustin/go-humanize"
	explorer "gitlab.com/ongresinc/labs/aws-explorer"
)

func volToCell(v explorer.DiskVolume) []*simpletable.Cell {
	return []*simpletable.Cell{
		{Align: simpletable.AlignCenter, Text: v.Region},
		{Align: simpletable.AlignCenter, Text: v.ID},
		{Align: simpletable.AlignCenter, Text: getKey(v.Tags, "Name")},
		{Align: simpletable.AlignCenter, Text: v.Type},
		{Align: simpletable.AlignCenter, Text: v.State},
		{Align: simpletable.AlignCenter, Text: humanize.IBytes(uint64(v.Size))},
		{Align: simpletable.AlignLeft, Text: processAttachment(v.Attachments)},
	}
}

func processAttachment(attachList []explorer.VolAttachment) string {
	var out []string
	for _, attach := range attachList {
		out = append(out, attach.InstanceID)
	}

	return strings.Join(out, "\n")
}

func printVolumes(volumes []explorer.DiskVolume) {

	table := simpletable.New()

	table.Header = &simpletable.Header{
		Cells: []*simpletable.Cell{
			{Align: simpletable.AlignCenter, Text: "Region"},
			{Align: simpletable.AlignCenter, Text: "VolumeID"},
			{Align: simpletable.AlignCenter, Text: "Name"},
			{Align: simpletable.AlignCenter, Text: "Type"},
			{Align: simpletable.AlignCenter, Text: "State"},
			{Align: simpletable.AlignCenter, Text: "Size"},
			{Align: simpletable.AlignCenter, Text: "Attachment"},
		},
	}

	var sizeOverall uint64
	var sizeUnused uint64

	for _, v := range volumes {
		table.Body.Cells = append(table.Body.Cells, volToCell(v))
		sizeOverall += uint64(v.Size)

		if v.State == "available" {
			sizeUnused += uint64(v.Size)
		}
	}

	printTable("All EC2 Volumes", table)
	if len(table.Body.Cells) > 0 {
		fmt.Printf("Unused size: %s\n", humanize.IBytes(sizeUnused))
		fmt.Printf("Total size: %s\n", humanize.IBytes(sizeOverall))
	}
}
